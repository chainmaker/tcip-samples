module demo

go 1.14

require (
	chainmaker.org/chainmaker/tcip-go v0.0.0-20220601151731-efc6af22b0cb
	github.com/golang/protobuf v1.4.2
	github.com/hyperledger/fabric-contract-api-go v1.1.1
)
