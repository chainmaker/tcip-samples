package main

import (
	"fmt"
	"encoding/json"
	"os"

	"chainmaker.org/chainmaker/tcip-go/v2/common/cross_chain"
)

func main() {
	chainConfigJson, _ := os.ReadFile("chain_config.json")
	var chainConfig cross_chain.ChainIdentityRequest
	err := json.Unmarshal([]byte(chainConfigJson), &chainConfig)
	if err != nil {
		fmt.Println(err)
		return
	}
	sign1Cert, _ := os.ReadFile("config/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/signcerts/Admin@org1.example.com-cert.pem")
	sign1Key, _ := os.ReadFile("config/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore/priv_sk")
	sign2Cert, _ := os.ReadFile("config/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp/signcerts/Admin@org2.example.com-cert.pem")
	sign2Key, _ := os.ReadFile("config/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp/keystore/priv_sk")
	tlsCert, _ := os.ReadFile("config/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt")
	tlsKey, _ := os.ReadFile("config/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key")
	peer1Ca, _ := os.ReadFile("config/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt")
	peer2Ca, _ := os.ReadFile("config/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt")
	ordererCa, _ := os.ReadFile("config/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt")
	chainConfig.FabricConfig.Org[0].SignCert = string(sign1Cert)
	chainConfig.FabricConfig.Org[0].SignKey = string(sign1Key)
	chainConfig.FabricConfig.Org[0].Peers[0].TrustRoot[0] = string(peer1Ca)
	chainConfig.FabricConfig.Org[1].SignCert = string(sign2Cert)
	chainConfig.FabricConfig.Org[1].SignKey = string(sign2Key)
	chainConfig.FabricConfig.Org[1].Peers[0].TrustRoot[0] = string(peer2Ca)
	//chainConfig.FabricConfig.SignCert = string(signCert)
	//chainConfig.FabricConfig.SignKey = string(signKey)
	chainConfig.FabricConfig.TlsCert = string(tlsCert)
	chainConfig.FabricConfig.TlsKey = string(tlsKey)
	//chainConfig.FabricConfig.Peers[0].TrustRoot[0] = string(peerCa)
	chainConfig.FabricConfig.Orderers[0].TrustRoot[0] = string(ordererCa)
	chainConfigByte, _ := json.Marshal(chainConfig)
	fmt.Printf(string(chainConfigByte))
}