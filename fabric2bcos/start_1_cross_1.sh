#!/bin/bash

VERSION=$(cat ../version.txt)

if [ ! -e "./chainmaker-go-run/chainmaker/bin/chainmaker" ];then
    echo "请下载chainmaker的二进制文件到chainmaker-go-run/chainmaker/bin文件夹下，下载地址：https://git.chainmaker.org.cn/chainmaker/chainmaker-go/uploads/b7f6f31a96334c4ea49aab0d680bbf75/chainmaker-v2.3.2-linux-x86_64.tar.gz，并解压命名为chainmaker"
    exit 1
fi

if [ ! -e "./chainmaker-go-run/cmc" ];then
    echo "请下载cmc的二进制文件到chainmaker-go-run文件夹下，下载地址：https://git.chainmaker.org.cn/chainmaker/chainmaker-go/uploads/0d820ef4c4864db6b8b9b28540564fa2/cmc-v2.3.2-linux-x86_64.tar.gz，并解压命名为cmc"
    exit 1
fi

if [ ! -e "./bcos-run/fisco-bcos" ];then
    echo "下载fisco-bcos二进制文件，如果失败请手动下载，下载地址：https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/tcip/fisco-bcos-v2.9.0-amd64.zip?inline=false，并放置于bcos-run文件夹下，解压并重命名为fisco-bcos"
    cd bcos-run
    wget https://git.chainmaker.org.cn/chainmaker/chainmaker-tools/-/raw/tcip/fisco-bcos-v2.9.0-amd64.zip?inline=false -O fisco-bcos.zip
    unzip fisco-bcos.zip
    mv fisco-bcos-v2.9.0-amd64 fisco-bcos
    chmod +x fisco-bcos
    cd ..
fi

CODEURL="git.chainmaker.org.cn"
ISTAG="tags"

if [ $TCIP_TEST ];then
  CODEURL="git.code.tencent.com"
  ISTAG="origin"
fi

function startChainmaker() {
#  if [ ! -d "./chainmaker-go" ];then
#    git clone git@git.code.tencent.com:ChainMaker/chainmaker-go.git
#  else
#    echo "chainmaker-go文件夹已经存在"
#  fi
#
#  cd chainmaker-go
#  git checkout -b v2.3.1_qc origin/v2.3.1_qc
#  make
#  cp bin/chainmaker ../chainmaker-go-run/chainmaker-v2.3.0_alpha-wx-org.chainmaker.org//bin/
#  cd tools/cmc
#  go build
#  cp cmc ../../../chainmaker-go-run/
  cd chainmaker-go-run/
  ./start.sh
  cd ..
}

function startFabric() {
  if [ ! -d "./fabric-samples" ];then
    curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.2.0 1.4.7
  else
    echo "fabric-samples文件夹已经存在"
  fi

  cp -rf crosschain1 $GOPATH/src/crosschain1
  cd fabric-samples/test-network
  ./network.sh up
  sleep 3
  ./network.sh createChannel
  sleep 3
  ./network.sh deployCC -ccn crosschain1 -ccp ../../crosschain1 -ccl go
  rm -rf $GOPATH/src/crosschain1
  cp -rf organizations ../../tcip-fabric-1-run/config/
  cd ../..
  docker ps -a | grep "peer\|orderer"
}

function startTcipRelayer() {
  if [ ! -d "./tcip-relayer" ];then
    git clone https://$CODEURL/chainmaker/tcip-relayer.git
  else
    echo "tcip-relayer文件夹已经存在"
  fi

  cd tcip-relayer
  git checkout -b $VERSION $ISTAG/$VERSION
  git pull
  make build
  cp tcip-relayer ../tcip-relayer-1-run/
  cd ../tcip-relayer-1-run/
  ./start.sh
  cd ..
}

function startTcipFabric() {
  if [ ! -d "./tcip-fabric" ];then
    git clone https://$CODEURL/chainmaker/tcip-fabric.git
  else
    echo "tcip-fabric文件夹已经存在"
  fi

  cd tcip-fabric
  git checkout -b $VERSION $ISTAG/$VERSION
  git pull
  make build
  cp tcip-fabric ../tcip-fabric-1-run

  cd ../tcip-fabric-1-run/
  ./start.sh

  cd ..
}

function startBcos() {
  cd bcos-run
  ./start.sh
  cd ..
}

function startTcipBcos() {
  if [ ! -d "./tcip-bcos" ];then
    git clone https://$CODEURL/chainmaker/tcip-bcos.git
  else
    echo "tcip-bcos文件夹已经存在"
  fi

  cd tcip-bcos
  git checkout -b $VERSION $ISTAG/$VERSION
  git pull
  make build
  cp tcip-bcos ../tcip-bcos-2-run

  cd ../tcip-bcos-2-run/
    ./start.sh

  cd ..
}

startChainmaker
startFabric
startBcos
startTcipRelayer
startTcipFabric
startTcipBcos