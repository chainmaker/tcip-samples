#!/bin/bash

nohup ./tcip-fabric start -c ./config/tcip_fabric.yml > panic.log 2>&1 & echo $! > pid

./tcip-fabric register -c config/tcip_fabric.yml

sleep 5

REGISTER=$(cat register.json)
curl -k -H "Content-Type: application/json" -X POST -d "$REGISTER" https://localhost:19999/v1/GatewayRegister

ADDRESS1=$(cat ../bcos-run/address | sed -n "1p")
sed -i "s/ADDRESS1/$ADDRESS1/g" event_1_cross_1.json
cd ../build_chain_config
go build -o ../tcip-fabric-1-run/main
cd ../tcip-fabric-1-run/
CHAINCONFIG=$(./main)
curl -k -H "Content-Type: application/json" -X POST -d "$CHAINCONFIG" https://localhost:19996/v1/ChainIdentity

sleep 5

EVENT=$(cat event_1_cross_1.json)
curl -k -H "Content-Type: application/json" -X POST -d "$EVENT" https://localhost:19996/v1/CrossChainEvent

echo "tcip-fabric-1 start"