module main

go 1.16

require (
	github.com/FISCO-BCOS/go-sdk v1.0.0
	github.com/ethereum/go-ethereum v1.9.16
	github.com/sirupsen/logrus v1.8.1
)
