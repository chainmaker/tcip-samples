#!/bin/bash

./stop.sh

rm -rf ./log
rm -rf ./data


#rm -rf chainmaker-go tcip-relayer tcip-chainmaker
rm -rf chainmaker-go-run/chainmaker/data/
rm tcip-fabric-1-run/tcip-fabric
rm tcip-bcos-2-run/tcip-bcos
rm tcip-fabric-1-run/main
rm -rf */*.log
rm -rf */*.log.*
rm -rf */logs
rm -rf */register.json
rm tcip-relayer-1-run/tcip-relayer
rm -rf tcip-fabric-1-run/database
rm -rf tcip-bcos-2-run/database
rm -rf bcos-run/*/data
rm -rf bcos-run/address
git checkout .