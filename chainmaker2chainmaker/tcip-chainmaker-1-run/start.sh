#!/bin/bash
./tcip-chainmaker register -c config/tcip_chainmaker.yml

REGISTER=$(cat register.json)
curl -k -H "Content-Type: application/json" -X POST -d "$REGISTER" https://localhost:19999/v1/GatewayRegister

./tcip-chainmaker spv -c config/tcip_chainmaker.yml \
-v 1.0 \
-p ./contract/spv.7z \
-r DOCKER_GO \
-P "{}" \
-C chainmaker001 \
-O install

nohup ./tcip-chainmaker start -c ./config/tcip_chainmaker.yml > panic.log 2>&1 & echo $! > pid

sleep 5

CHAINCONFIG=$(cat chain_config.json)
curl -k -H "Content-Type: application/json" -X POST -d "$CHAINCONFIG" https://localhost:19996/v1/ChainIdentity

sleep 5

if [ "$1" = "1" ]; then
  EVENT=$(cat event_1_cross_1.json)
  curl -k -H "Content-Type: application/json" -X POST -d "$EVENT" https://localhost:19996/v1/CrossChainEvent
else
  EVENT=$(cat event_1_cross_2.json)
    curl -k -H "Content-Type: application/json" -X POST -d "$EVENT" https://localhost:19996/v1/CrossChainEvent
fi

echo "tcip-chainmaker-1 start"