#!/bin/bash

./stop.sh

rm -rf ./log
rm -rf ./data


#rm -rf chainmaker-go tcip-relayer tcip-chainmaker
rm -rf chainmaker-go-run/chainmaker/data/
rm tcip-chainmaker-1-run/tcip-chainmaker
rm -rf */*.log
rm -rf */*.log.*
rm -rf */logs
rm -rf */register.json
rm tcip-chainmaker-2-run/tcip-chainmaker
rm tcip-chainmaker-3-run/tcip-chainmaker
rm tcip-relayer-1-run/tcip-relayer
rm tcip-relayer-2-run/tcip-relayer
rm -rf tcip-chainmaker-1-run/database
rm -rf tcip-chainmaker-2-run/database
rm -rf tcip-chainmaker-3-run/database